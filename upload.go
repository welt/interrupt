package main

import (
	"fmt"
	"io"
	"os"
)

func uploadHandler(a string) error {
	uploadKey, err := getUploadKey()
	if err != nil {
		return err
	}

	var reader io.Reader

	if a == "-" {
		reader = os.Stdin
	} else {
		reader, err = os.Open(a)
		if err != nil {
			return err
		}
	}

	itCl.UploadKey = uploadKey

	r, err := itCl.UploadFile(reader, a)
	if err != nil {
		return err
	}

	fmt.Println(r)
	return nil
}
