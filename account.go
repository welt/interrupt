package main

import (
  "errors"
  "fmt"
  "gitea.com/webb/teal"
)

func getAccountHandler(s string) (error){
  username, password, err := getLoginDetails()
  if err != nil {
    return err
  }

  c, err := teal.Login(username, password)
  if err != nil {
    return err
  }

  switch s {
  case "key":
    fmt.Println(c.User.UploadKey)
    return nil
  }
  return errors.New("internal: no fetchable attribute found")
}
