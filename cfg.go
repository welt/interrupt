package main

import (
	"errors"
	"os"
)

func getUploadKey() (string, error) {
	rdKey := os.Getenv("PXL_KEY")
	if rdKey != "" {
		return rdKey, nil
	}

	rdKey = os.Getenv("MIRAGE_KEY")
	if rdKey != "" {
		return rdKey, nil
	}

	return "", errors.New("upload key not set")
}

func getLoginDetails() (string, string, error) {
	rdUsr, rdPsswd := os.Getenv("PXL_USERNAME"), os.Getenv("PXL_PASSWORD")
	if rdUsr != "" && rdPsswd != "" {
		return rdUsr, rdPsswd, nil
	}

	if rdUsr == "" && rdPsswd == "" {
		return "", "", errors.New("user credentials not set")
	}

	if rdUsr == "" {
		return "", "", errors.New("username not set")
	}

	return "", "", errors.New("password not set")

}
