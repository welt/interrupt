package main

import (
	"gitea.com/webb/teal"
	"github.com/urfave/cli"
	"os"
)

var itCl = teal.Client{
	UserAgent: "interrupt v0.0.0",
}

func main() {
	cli := &cli.App{
    Name: "interrupt",
    Usage: "a simple CLI for pxl.blue",
    Description: "This is a simple CLI for pxl.blue. You can upload images, get account information and more.",
		Commands: []*cli.Command{
			{
				Name:    "upload",
				Aliases: []string{"u"},
				Usage:   "upload a file",
				Action: func(c *cli.Context) error {
          err := uploadHandler(c.Args().First())
					return err
				},
			},
			{
				Name:    "get",
				Aliases: []string{"g"},
				Usage:   "get information from your account",
				Subcommands: []*cli.Command{
					{
						Name:  "key",
						Usage: "get your upload key by logging in with your username and password",
						Action: func(c *cli.Context) error {
							err := getAccountHandler("key")
							return err
						},
					},
				},
			},
		},
	}

	err := cli.Run(os.Args)
	if err != nil {
    os.Stderr.WriteString("interrupt: " + err.Error() + "\n")
    os.Exit(1)
	}
}
